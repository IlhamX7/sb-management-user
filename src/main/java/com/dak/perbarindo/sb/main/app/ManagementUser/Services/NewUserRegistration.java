package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.TripleDES;
import com.dak.perbarindo.sbdbcontroller.gateway.model.AppSerialNumber;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Configuration;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Lembaga;
import com.dak.perbarindo.sbdbcontroller.gateway.model.ResponseMessage;
import com.dak.perbarindo.sbdbcontroller.gateway.model.UserApp;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import org.apache.catalina.User;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class NewUserRegistration {


    public static Boolean isFormValidFromEmptyAttribut(JSONObject reqBody){
        if(reqBody.getString("nik").isEmpty())return false;
        if(reqBody.getString("password").isEmpty())return false;
        if(reqBody.getString("phone_number").isEmpty())return false;
        if(reqBody.getString("fullname").isEmpty())return false;
        if(reqBody.getString("email").isEmpty())return false;
        if(reqBody.getString("username").isEmpty())return false;
        return true;
    }

    public static JSONObject checkUnique(JSONObject reqBody) throws Exception{
        System.out.println("asd checkUnique Func");
         List<Object> response;
         TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);
        System.out.println("asd nik "+tripleDES.encrypt("3217022803960003"));

         //cek username
         response = (List<Object>)SInit.getUserAppDao().getEm().createNativeQuery("select username from user_app where username=:pusername")
                 .setParameter("pusername",reqBody.getString("username")).getResultList();
         System.out.println("asd size "+response.size());
         if(!response.isEmpty()){
             System.out.println("asd masuk 2");
             ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("username_exist");
             return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
         }

         //cek email
        response = (List<Object>)SInit.getUserAppDao().getEm().createNativeQuery("select email from user_app where email=:pemail")
                .setParameter("pemail",reqBody.getString("email")).getResultList();

        if(response.size()>0){
            ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("email_exist");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //cek NIK
        response = (List<Object>)SInit.getUserAppDao().getEm().createNativeQuery("select nik from user_app where nik=:pnik")
                .setParameter("pnik",reqBody.getString("nik")).getResultList();
        if(response.size()>0){
            ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("nik_exist");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //cek Phone number
        response = (List<Object>)SInit.getUserAppDao().getEm().createNativeQuery("select phone_number from user_app where phone_number=:pno")
                .setParameter("pno",reqBody.getString("phone_number")).getResultList();
        if(response.size()>0){
            ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("phone_number_exist");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS,Constants.RM_SUCCESS);
    }

    public static JSONObject formValidation(JSONObject reqBody) throws Exception{
        System.out.println("asd formValidation Func");
        final TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);

        //validate username
        final String username = tripleDES.decrypt(reqBody.getString("username"));
        if(username.length()<5 || username.contains(" ") || username.length()>20){
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("username_validation");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //validate password
        final String password = tripleDES.decrypt(reqBody.getString("password"));
        if(password.length()<5 || password.length()>30 || !Constants.isAlphanumeric(password)
                || !Constants.isContainCapital(password) || !Constants.isContainSpecialCharacter(password)){
            if(!Constants.isAlphanumeric(password)) System.out.println("asd alpha");
            if(!Constants.isContainSpecialCharacter(password)) System.out.println("asd special cha");

            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("password_validation");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //email
        final String email = reqBody.getString("email");
        if(!Constants.isValidEmail(email)){
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("email_validation");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //nik
        final String nik = tripleDES.decrypt(reqBody.getString("nik"));
        if(!nik.matches("[0-9]+") || nik.length()!=16){
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("nik_validation");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //phone number
        final String phone_number = reqBody.getString("phone_number");
        if(!phone_number.matches("[0-9]+") || phone_number.length()<10 || phone_number.length()>15){
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("phone_number_validation");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //full name
        final String full_name = reqBody.getString("fullname");
        if(full_name.length()<5 || full_name.length()>25){
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("fullname_validation");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS,Constants.RM_SUCCESS);
    }

    public static JSONObject createNewUser(JSONObject reqBody){
        final JSONObject userData = reqBody.getJSONObject("user");
        final JSONObject terminalData = reqBody.getJSONObject("terminal");
        UserApp UserApp = new UserApp();
        UserApp.setUsername(userData.getString("username"));
        UserApp.setPassword(userData.getString("password"));
        UserApp.setEmail(userData.getString("email"));
        UserApp.setNik(userData.getString("nik"));
        UserApp.setPhoneNumber(userData.getString("phone_number"));
          
        //UserApp.setUserType(userData.getInt("privillage"));
        try {
            final Object user =  SInit.getUserAppDao().getEm().createQuery("select u from UserApp u where u.bankId.bankId=:pbankid and u.userType=1")
                    .setParameter("pbankid", terminalData.getLong("bank_id"))
                    .getSingleResult();
            UserApp.setUserType(0);
        }catch (Exception e){
            e.printStackTrace();
            UserApp.setUserType(1);
        }
        if(UserApp.getUserType()==1){
            UserApp.setStatus(1);
        }else{
            UserApp.setStatus(0);
        }
        UserApp.setFullname(userData.getString("fullname"));
        UserApp.setStatusLogin(0);

        UserApp.setCrTime(new Date());
        UserApp.setFailLogin(0);
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 3);
        UserApp.setExpiredPassword(cal.getTime());
        Lembaga Lembaga;
        if(terminalData.has("bank_id")) {
            Lembaga = SInit.getLembagaDao().findByBankId(terminalData.getLong("bank_id"));
        }else if(terminalData.has("hostname") && terminalData.has("proc_id")){
            final String bankId = SInit.getLembagaDao().findByHostAndProcId(terminalData.getString("hostname"),terminalData.getString("proc_id"));
            Lembaga = SInit.getLembagaDao().findByBankId((long)Integer.parseInt(bankId));
        }else{
            final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("user_registration_failed");
            return ResponseGenerator.ResponseGenerator(rm.getRc(),rm.getRm());
        }
        UserApp.setBankId(Lembaga);
        SInit.getUserAppDao().saveorupdate(UserApp);
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS,Constants.RM_SUCCESS);
    }

}
