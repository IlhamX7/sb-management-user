package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.TripleDES;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Configuration;
import com.dak.perbarindo.sbdbcontroller.gateway.model.ResponseMessage;
import com.dak.perbarindo.sbdbcontroller.gateway.model.UserApp;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import com.mailjet.client.ClientOptions;
import com.mailjet.client.MailjetClient;
import com.mailjet.client.transactional.*;
import com.mailjet.client.transactional.response.SendEmailsResponse;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

public class AccountManagement {

    static final Logger log = LoggerFactory.getLogger("AccountManagement");

    public static JSONObject getOTP(JSONObject reqBody) throws Exception {
        UserApp user = SInit.getUserAppDao().findUserById(reqBody.getJSONObject("user").getInt("user_id"));
        if (user.getStatus() == 1)
        {
            String random = Constants.getRandomNumberString();
            user.setExpiredPassword(new Date());
            final TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);
            user.setPassword(tripleDES.encrypt(random));
            log.info("asd encrypted OTP " + tripleDES.encrypt(random));
            log.info("asd encrypted OTP " + tripleDES.encrypt(random));
            SInit.getUserAppDao().saveorupdate(user);
            return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, new JSONObject().put("otp_password", random));
        }
        return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
    }

    public static JSONObject resetPassword(JSONObject reqBody) throws Exception {

        TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);
        UserApp user = SInit.getUserAppDao().findUserByBankIdNUsername(reqBody.getJSONObject("user").getString("username"), reqBody.getJSONObject("terminal").getLong("bank_id"));
        if (user != null)
        {

            //Inactive account
            if (user.getStatus() == 0)
            {
                final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("account_inactive");
                return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm());
            }

            //Blocked account
            if (user.getStatus() == 2)
            {
                final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("max_login_attempt");
                return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm());
            }

            //Validate password
            final String password = tripleDES.decrypt(reqBody.getJSONObject("user").getString("new_password"));
            if (password.length() < 5 || password.length() > 30 || !Constants.isAlphanumeric(password)
                    || !Constants.isContainCapital(password) || !Constants.isContainSpecialCharacter(password))
            {
                if (!Constants.isAlphanumeric(password))
                {
                    log.info("asd alpha");
                }
                if (!Constants.isContainSpecialCharacter(password))
                {
                    log.info("asd special cha");
                }
                final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("password_validation");
                return ResponseGenerator.ResponseGenerator(responseMessage.getRc(), responseMessage.getRm());
            }
            log.info("asd lewat 1 mor");
            log.info("asd 1 " + user.getPassword());
            log.info("asd 2 " + reqBody.getJSONObject("user").getString("old_password"));
            if (user.getPassword().equals(reqBody.getJSONObject("user").getString("old_password")))
            {
                user.setPassword(reqBody.getJSONObject("user").getString("new_password"));
                Configuration conf = SInit.getConfigurationDao().findByConfigName("expired_date_month");
                user.setExpiredPassword(DateUtils.addMonths(new Date(), Integer.parseInt(conf.getValue())));
                user.setFailLogin(0);
                user.setStatus(1);
                SInit.getUserAppDao().saveorupdate(user);
                return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS);
            }
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
        }
        final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("unregistered_user_on_terminal");
        return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm());
    }

    public static JSONObject getAllUserByBankId(JSONObject reqBody) {
        final List<UserApp> listUser = SInit.getUserAppDao().findAllUserByBankId(reqBody.getInt("bank_id"));
        JSONArray array = new JSONArray();
        JSONObject data;
        log.info("asd ");
        for (UserApp user : listUser)
        {
            data = new JSONObject();
            data.put("user_id", user.getUserId());
            data.put("username", user.getUsername());
            data.put("fullname", user.getFullname());
            data.put("nik", user.getNik());
            data.put("email", user.getEmail());
            data.put("no_hp", user.getPhoneNumber());
            data.put("expired_password", user.getExpiredPassword());
            data.put("status", user.getStatus());
            array.put(data);
        }
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, array);
    }

    public static JSONObject activateUser(JSONObject reqBody) {
        UserApp user = SInit.getUserAppDao().findUserById(reqBody.getInt("user_id"));
        user.setStatus(1);
        user.setFailLogin(0);
        SInit.getUserAppDao().saveorupdate(user);
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS);
    }

    public static JSONObject deactiveUser(JSONObject reqBody) {
        UserApp user = SInit.getUserAppDao().findUserById(reqBody.getInt("user_id"));
        user.setStatus(0);
        SInit.getUserAppDao().saveorupdate(user);
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS);
    }

    public static JSONObject tesEMail(String email) throws Exception {
        String emailHtml = SInit.getConfigurationDao().findByConfigName("forget_password_theme").getDescription();
        ClientOptions options = ClientOptions.builder()
                .apiKey("4b490dee1673c324e82a232d591627d8")
                .apiSecretKey("4511bce5ec2f3f6ea93e317f81d128f1")
                .build();

        MailjetClient client = new MailjetClient(options);
        TransactionalEmail message1 = TransactionalEmail
                .builder()
                .to(new SendContact(email.trim(), "TESTING"))
                .from(new SendContact("noreply@perbarindo.org", "Perbarindo"))
                .htmlPart(emailHtml)
                .subject("FORGOT PASSWORD APP SHARING BANDWIDTH")
                .trackOpens(TrackOpens.ENABLED)
                //.attachment(Attachment.fromFile(attachmentPath))
                .header("test-header-key", "test-value")
                .customID("custom-id-value")
                .build();

        SendEmailsRequest request = SendEmailsRequest
                .builder()
                .message(message1) // you can add up to 50 messages per request
                .build();

        // act
        SendEmailsResponse response = request.sendWith(client);
        log.info("asd response " + response);
        return ResponseGenerator.ResponseGenerator("00", response.toString());

    }

    public static JSONObject sendEmail(String email, String emailHtml, String title) throws Exception {
        ClientOptions options = ClientOptions.builder()
                .apiKey("4b490dee1673c324e82a232d591627d8")
                .apiSecretKey("4511bce5ec2f3f6ea93e317f81d128f1")
                .build();

        MailjetClient client = new MailjetClient(options);
        TransactionalEmail message1 = TransactionalEmail
                .builder()
                .to(new SendContact(email.trim(), "TESTING"))
                .from(new SendContact("noreply@perbarindo.org", "Perbarindo"))
                .htmlPart(emailHtml)
                .subject(title)
                .trackOpens(TrackOpens.ENABLED)
                //.attachment(Attachment.fromFile(attachmentPath))
                .header("test-header-key", "test-value")
                .customID("custom-id-value")
                .build();

        SendEmailsRequest request = SendEmailsRequest
                .builder()
                .message(message1) // you can add up to 50 messages per request
                .build();

        // act
        SendEmailsResponse response = request.sendWith(client);
        log.info("asd response " + response);
        return ResponseGenerator.ResponseGenerator("00", response.toString());

    }

    public static JSONObject forgetPassword2(JSONObject reqBody) throws Exception {
        try
        {
            
            log.info("forget password process running...");
            
            final TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);
            final String nik = tripleDES.encrypt(reqBody.getString("nik"));
            UserApp user = SInit.getUserAppDao().findUserByNIK(nik.trim());
            log.info("user: " + user.getFullname() + ", bankid:" + user.getBankId().getBankId());
            if (user == null)
            {
                final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("nik_not_found");
                return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm());
            }
            if (user.getUserType() == 1)
            {
                user.setStatus(1);
                user.setFailLogin(0);
            }
            final String otp = Constants.getRandomNumberString();
            user.setPassword(tripleDES.encrypt(otp));
            user.setExpiredPassword(new Date());
            SInit.getUserAppDao().saveorupdate(user);
            String emailHtml = SInit.getConfigurationDao().findByConfigName("forget_password_theme").getDescription();
            emailHtml = emailHtml.replace("#nama_bpr", user.getBankId().getBankName());
            emailHtml = emailHtml.replace("#username", tripleDES.decrypt(user.getUsername()));
            emailHtml = emailHtml.replace("#password", otp);

            //start of mailjet
        ClientOptions options = ClientOptions.builder()
                .apiKey("4b490dee1673c324e82a232d591627d8")
                .apiSecretKey("4511bce5ec2f3f6ea93e317f81d128f1")
                .build();

        MailjetClient client = new MailjetClient(options);
        TransactionalEmail message1 = TransactionalEmail
                .builder()
                .to(new SendContact(user.getEmail().trim(), user.getFullname()))
                .from(new SendContact("noreply@perbarindo.org", "Perbarindo"))
                .htmlPart(emailHtml)
                .subject("FORGOT PASSWORD APP SHARING BANDWIDTH")
                .trackOpens(TrackOpens.ENABLED)
                //.attachment(Attachment.fromFile(attachmentPath))
                .header("test-header-key", "test-value")
                .customID("custom-id-value")
                .build();

        SendEmailsRequest request = SendEmailsRequest
                .builder()
                .message(message1) // you can add up to 50 messages per request
                .build();

        // act
        SendEmailsResponse response = request.sendWith(client);
        log.info("asd response " + response);
        // end of mailjet
        sendWA(user.getPhoneNumber(), "*SHARING BANDWIDTH* \n Username : " + tripleDES.decrypt(user.getUsername()) + "\n Password : " + otp + "\n\n Login menggunakan username dan password tersebut untuk mereset password. Masukkan password lama anda dengan password diatas kemudian isikan password baru anda pada kolom berikutnya");
        ;
        final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("reset_password_success");
        return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm().replace("emails", user.getEmail()));
            

//            JSONObject supportResponse = SupportServices.broadcastResetPassword(
//                    user.getEmail().trim(),
//                    user.getBankId().getBankName(),
//                    tripleDES.decrypt(user.getUsername()),
//                    otp
//            );
//
//            if (supportResponse.getString("rc").equals("00"))
//            {
//                log.info("success sending broadcast");
//                final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("reset_password_success");
//                return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm().replace("emails", user.getEmail()));
//            } else
//            {
//                log.info("failed sending broadcast");
//                return ResponseGenerator.ResponseGenerator("99", "Terjadi Kesalahan [amfp2]");
//            }
        } catch (Exception e)
        {
            log.error(new String(), e);
            return ResponseGenerator.ResponseGenerator("99", "Terjadi Kesalahan [amfp2-exc]");
        }
    }

    public static JSONObject sendNotifPenagihan(JSONObject reqBody) throws Exception {
        final String message = "Yth BPR " + reqBody.getString("nama_bpr") + "\nBerikut kode pembayaran untuk bulan " + reqBody.getString("bulan") + "sejumlah " + reqBody.getString("nominal_tagihan") + " silakan bayar via pembayaran telepon telkom dengan kode: 0211131xxxx (via channel ATM, MBANKING, INTERNET BANKING, POS, PEGADAIAN, ALFAMART)\n\n terimakasih";
        sendWA(reqBody.getString("phone"), message);
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS);
    }

    private static void sendWA(String phone, String message) throws Exception {

        phone = phone.replace("+", "");
        if (String.valueOf(phone.charAt(0)).equals("0"))
        {
            phone = "62" + phone.substring(1);
        }
        final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        final HttpPost httpPost = new HttpPost(Constants.URL_NOTIF_WA);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("device_number", "6287720009216");
        jsonObject.put("message", message);
        jsonObject.put("to", phone);
        StringEntity se = new StringEntity(jsonObject.toString());
        httpPost.setEntity(se);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("access-token", Constants.TOKEN_WA);
        HttpResponse response = httpClient.execute(httpPost);
        final BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
        String line;
        final StringBuilder respMsg = new StringBuilder();
        while ((line = br.readLine()) != null)
        {
            respMsg.append(line);
        }
        final JSONObject jsonResponseAPI = new JSONObject(respMsg.toString());
        log.info("RESPONSE WA " + jsonResponseAPI.toString());
        log.info("RESPONSE WA " + jsonResponseAPI.toString());
    }

    public static JSONObject forgetPassword(JSONObject reqBody) throws Exception {

        final TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);
        final String nik = tripleDES.encrypt(reqBody.getString("nik"));
        UserApp user = SInit.getUserAppDao().findUserByNIK(nik.trim());
        if (user == null)
        {
            final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("nik_not_found");
            return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm());
        }
        if (user.getUserType() == 1)
        {
            user.setStatus(1);
            user.setFailLogin(0);
        }
        final String otp = Constants.getRandomNumberString();
        user.setPassword(tripleDES.encrypt(otp));
        user.setExpiredPassword(new Date());
        SInit.getUserAppDao().saveorupdate(user);
        String emailHtml = SInit.getConfigurationDao().findByConfigName("forget_password_theme").getDescription();
        emailHtml = emailHtml.replace("#nama_bpr", user.getBankId().getBankName());
        emailHtml = emailHtml.replace("#username", tripleDES.decrypt(user.getUsername()));
        emailHtml = emailHtml.replace("#password", otp);

        final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        final HttpPost httpPost = new HttpPost(Constants.URL_SEND_EMAIL);
        final JSONObject jsonParam = new JSONObject();
        JSONObject tempObj = new JSONObject();
        tempObj.put("email", "noreply@perbarindo.org");
        tempObj.put("name", "PERBARINDO");
        jsonParam.put("from", tempObj);
        tempObj = new JSONObject();
        tempObj.put("email", user.getEmail().trim());
        tempObj.put("name", user.getFullname().trim());
        jsonParam.put("to", tempObj);
        jsonParam.put("subject", "CHANGE PASSWORD");
        jsonParam.put("textpart", "");
        jsonParam.put("html", emailHtml);
        StringEntity se = new StringEntity(jsonParam.toString());
        httpPost.setEntity(se);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("api-key", Constants.HEADER_1_SEND_EMAIL);
        httpPost.setHeader("mailjet_user", Constants.HEADER_2_SEND_EMAIL);
        httpPost.setHeader("mailjet_pass", Constants.HEADER_3_SEND_EMAIL);
        HttpResponse response = httpClient.execute(httpPost);
        final BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
        String line;
        final StringBuilder respMsg = new StringBuilder();
        while ((line = br.readLine()) != null)
        {
            respMsg.append(line);
        }
        final JSONObject jsonResponseAPI = new JSONObject(respMsg.toString());
        if (jsonResponseAPI.getString("message").contains("Successfully"))
        {
            final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("reset_password_success");
            return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm().replace("emails", user.getEmail()));
        } else
        {
            return ResponseGenerator.ResponseGenerator(Constants.RC_IE, jsonResponseAPI.getString("message"));
        }
//        final MimeMessage mime = GmailQuickstart.createEmail(user.getEmail().trim(),Constants.EMAIL_PERBARINDO,"Reset Password",emailHtml);
//        GmailQuickstart.sendMessage(gmailService,"me",mime);

    }

    public static JSONObject checkInvoice(JSONObject reqBody) {
        final JSONObject response = new JSONObject();
//        final Logger log = LoggerFactory.getLogger("AccountManagement");
        JSONObject invoice = ExternalServices.getInvoice(reqBody);
        final JSONArray invoices;
        log.info(invoice.toString());
        if (invoice.getString("message").equals("Berhasil get invoice"))
        {
            invoices = invoice.getJSONArray("data");
            if (invoices.length() == 0)
            {
                return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Invoice not found");
            }
            invoice = invoices.getJSONObject(invoices.length() - 1);
            response.put("tagihan", invoice.getString("total"));
            if (invoice.isNull("payment_code"))
            {
                final JSONObject responseSendINQ = ExternalServices.sendInquiry(invoice.getString("invoice_number"));
                log.info(responseSendINQ.toString());
                if (responseSendINQ.getString("rc").equals(Constants.RC_SUCCESS))
                {
                    response.put("payment_code", responseSendINQ.getJSONObject("data").getString("payment_code"));
                }
            } else
            {
                response.put("payment_code", invoice.getString("payment_code"));
            }
            return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, response);
        } else
        {
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Invoice not found");
        }
    }

}
