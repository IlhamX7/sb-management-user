package com.dak.perbarindo.sb.main.app.ManagementUser;

import java.util.Properties;

import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.TripleDES;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.gmail.Gmail;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import static com.dak.perbarindo.sb.main.app.ManagementUser.Services.GmailQuickstart.*;
import static com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants.*;

@SpringBootApplication
@EnableScheduling
public class ManagementUserApplication {

    static Logger log = LoggerFactory.getLogger(ManagementUserApplication.class);

    public static void main(String[] args) {
        try
        {
            TripleDES tripleDES = new TripleDES(TDES_KEY_APP);
            System.out.println("nih-----" + tripleDES.encrypt("morten"));
            SpringApplication.run(ManagementUserApplication.class, args);
//            sshConnection();
            SInit spring = new SInit();
			spring.InitService("/src/main/resources/ApplicationContext.xml");
//            spring.InitService("/resources/ApplicationContext.xml");
            log.info(""
                    + "\n SERVICE STARTED::.");
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    

    private static void initGmailAPI() throws Exception {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        gmailService = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
        // Print the labels in the user's account.
    }

    private static void sshConnection() throws JSchException {
        final String host = "103.28.148.203";
        final String user = "root";
        final String password = "Qawsed#1477";
        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        final JSch jsch = new JSch();
        final Session session = jsch.getSession(user, host, 7789);
        session.setPassword(password);
        session.setConfig(config);
        session.connect();
        System.out.println("Connected");
        session.setPortForwardingL(5477, "10.81.20.87", 5432);
    }

}
