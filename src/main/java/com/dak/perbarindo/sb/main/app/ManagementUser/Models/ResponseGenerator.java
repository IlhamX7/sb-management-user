package com.dak.perbarindo.sb.main.app.ManagementUser.Models;

import org.json.JSONArray;
import org.json.JSONObject;

public class ResponseGenerator {
    public static JSONObject ResponseGenerator(String rc,String rm,JSONObject body){
        return new JSONObject().put("rc",rc).put("rm",rm).put("data",body);
    }

    public static JSONObject ResponseGenerator(String rc, String rm, JSONArray body){
        return new JSONObject().put("rc",rc).put("rm",rm).put("data",body);
    }

    public static JSONObject ResponseGenerator(String rc,String rm){
        return new JSONObject().put("rc",rc).put("rm",rm);
    }
}
