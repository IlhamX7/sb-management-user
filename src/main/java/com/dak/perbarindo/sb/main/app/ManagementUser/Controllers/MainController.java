package com.dak.perbarindo.sb.main.app.ManagementUser.Controllers;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import com.dak.perbarindo.sb.main.app.ManagementUser.Services.*;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.TripleDES;
import com.dak.perbarindo.sbdbcontroller.gateway.model.ResponseMessage;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Sessions;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import static com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants.internalServerError;

/**
 * @author Morten
 */
@RestController
public class MainController {

    final Logger log = LoggerFactory.getLogger(getClass());
    long startTime;
    String temp_log;

    //@Scheduled(initialDelay = 25000, fixedRate = 15000)
    public void run() {
        System.out.println(" asd masukkk");
        log.info("<<<<<<<<<< TASK>>>>>>>>>>");
        try
        {
            final String enable = SInit.getConfigurationDao().getValueByName(Constants.CONFIG_SCHEDULER);
            if (enable.equals("1"))
            {
                System.out.println("asd ngirim pesan WA");
                log.info("sending WA");
            }
//            final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
//            final HttpPost httpPost = new HttpPost(Constants.URL_NOTIF_WA);
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("device_number","6287720009216");
//            jsonObject.put("message","testing");
//            jsonObject.put("to","6285156973768");
//            StringEntity se = new StringEntity(jsonObject.toString());
//            httpPost.setEntity(se);
//
//            httpPost.setHeader("Content-Type", "application/json");
//            httpPost.setHeader("access-token", Constants.TOKEN_WA);
//            HttpResponse response = httpClient.execute(httpPost);
//            final BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
//            String line;
//            final StringBuilder respMsg = new StringBuilder();
//            while ((line = br.readLine()) != null) {
//                respMsg.append(line);
//            }
//            final JSONObject jsonResponseAPI = new JSONObject(respMsg.toString());
//            log.info("<<<<WHAT'S APP NOTIFICATIONS>>>>");
//            log.info(jsonResponseAPI.toString());

        } catch (Exception e)
        {
            log.info("");
        }
    }

    @Autowired
    private HttpServletRequest request;

    @Transactional
    @RequestMapping(value = "/sb/dukcapil/{path}", method = RequestMethod.POST)
    public String Dukcapil(@RequestBody(required = false) String body, @PathVariable("path") String path,
            HttpServletRequest request) throws ParseException {
        final Sessions sessions = new Sessions();

        sessions.setReqBody(body);
        sessions.setPath(path);
        sessions.setReqTime(new Date());
        JSONObject result;
        startTime = System.currentTimeMillis();
        try
        {
            final TripleDES des = new TripleDES(Constants.TDES_KEY_APP);
            log.info(".::INCOMING REQUEST::. \n*Path : " + path + "\n*Body\n" + des.encrypt(body) + "\n" + new Date());
            final JSONObject req = new JSONObject(body);
            final String response = ExternalServices.forward(req);
            saveLog(des.encrypt(response), sessions);
            return response;
        } catch (Exception e)
        {
            e.printStackTrace();
            log.error(e.getMessage());
            sessions.setRc(Constants.RC_IE);
            sessions.setException(e.getMessage());
            sessions.setRm(Constants.RM_IE);
            sessions.setResBody("Exception");
            sessions.setResTime(new Date());
            final Calendar timeEnd = Calendar.getInstance();
            sessions.setPerformanceTime(System.currentTimeMillis() - startTime);
            System.out.println("asd simpan");
            log.info(".::RESPONSE::. \n" + Constants.RM_IE);
            System.out.println("asd exception " + e.getMessage());
            SInit.getSessionsDao().saveorupdate(sessions);
            return internalServerError().toString();
        }
    }

    @Transactional
    @RequestMapping(value = "/sb/{path}", method = RequestMethod.POST)
    public String MainController(@RequestBody(required = false) String body, @PathVariable("path") String path,
            HttpServletRequest request) throws ParseException {

        log.info("==> Starting Process Request : " + path
                + "\nrequest body: " + body);
        //log.info("\n\n.::INCOMING REQUEST::. \n\n*Path : " + path + "\n\n*Body\n" + body);
        temp_log = "";
        temp_log = ".::INCOMING REQUEST::. \n*Path : " + path + "\n*Body\n" + body;
        final JSONObject req = new JSONObject(body);
        final Sessions sessions = new Sessions();
        try
        {

            //init session
            sessions.setReqBody(body);
            sessions.setPath(path);
            sessions.setReqTime(new Date());
            JSONObject result;
            startTime = System.currentTimeMillis();

            switch (path)
            {
                case "createNewUser":
                    //cek apakah terminal sudah terdaftar
                    result = ExternalServices.CheckTerminal(req.getJSONObject("terminal"));
                    if (result.getString("rc").equals("00"))
                    {
                        //cek unique
                        if (!NewUserRegistration.isFormValidFromEmptyAttribut(req.getJSONObject("user")))
                        {
                            saveLog("Form tidak lengkap", sessions);
                            return ResponseGenerator.ResponseGenerator(Constants.RC_IE, "Silahkan lengkapi data diri anda !").toString();
                        }
                        sessions.setBankId(result.getJSONObject("data").getLong("bank_id"));
                        result = NewUserRegistration.checkUnique(req.getJSONObject("user"));
                        if (result.getString("rc").equals("00"))
                        {
                            result = NewUserRegistration.formValidation(req.getJSONObject("user"));
                            if (result.getString("rc").equals("00"))
                            {
                                result = NewUserRegistration.createNewUser(req);
                                if (result == null)
                                {
                                    throw new NoResultException();
                                }
                                saveLog(result, sessions);
                                return result.toString();
                            } else
                            {
                                saveLog(result, sessions);

                                return result.toString();
                            }
                        } else
                        {
                            saveLog(result, sessions);
                            return result.toString();
                        }
                    } else
                    {
                        saveLog(result, sessions);
                        return result.toString();
                    }

                case "login":
                    result = ExternalServices.CheckTerminal(req.getJSONObject("terminal"));
                    log.info("check terminal result: " + result.toString());
                    System.out.println("asd result " + result);
                    if (result.getString("rc").equals("00"))
                    {
                        
//                        final JSONObject duedate = ExternalServices.checkDuedate(result.getJSONObject("data"));
                        final JSONObject duedate = new JSONObject()
                                .put("rc", "00")
                                .put("rm", new JSONObject().put("due_date", "2021-12-20"));
                        
                        log.info("duedate result: " + duedate.toString());
                        if (duedate.getString("rc").equals("00"))
                        {
                            System.out.println("asd resilt check " + duedate.toString());
                            if (result.getJSONObject("data").has("id"))
                            {
                                sessions.setUserId(result.getJSONObject("data").getLong("id"));
                            }
                            sessions.setBankId(result.getJSONObject("data").getLong("bank_id"));
                            result = UserAuth.CheckLogin(req.put("terminal_id", result.getJSONObject("data").getString("terminal_id")).put("duedate", duedate.getJSONObject("rm")));
                            log.info("user check login result: " + result);
                            saveLog(result, sessions);
                            if (result.getString("rc").equals("00"))
                            {
                                final JSONObject checkInvoice = ExternalServices.checkInvoice(new JSONObject().put("bank_id", sessions.getBankId()).put("duedate", duedate.getJSONObject("rm").getString("due_date")));
                                result.put("notif", checkInvoice.getString("rm"));
                                log.info("check invoice result: " + checkInvoice);
                            }
                            return result.toString();
                        }
                        final ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("tagihan_not_found");
                        saveLog(rm.getRm(), sessions);
                        return ResponseGenerator.ResponseGenerator(rm.getRc(), rm.getRm()).toString();
                    }
                    saveLog(result, sessions);
                    return result.toString();

                case "getInvoice":
                    result = AccountManagement.checkInvoice(req);
                    sessions.setBankId(req.getLong("bank_id"));
                    saveLog(result, sessions);
                    return result.toString();

                case "logout":
                    //cek apakah terminal sudah terdaftar
                    result = ExternalServices.CheckTerminal(req.getJSONObject("terminal"));
                    if (result.getString("rc").equals("00"))
                    {
                        if (result.getJSONObject("data").has("id"))
                        {
                            sessions.setUserId(result.getJSONObject("data").getLong("id"));
                        }
                        sessions.setBankId(result.getJSONObject("data").getLong("bank_id"));
                        result = UserAuth.logout(req.getJSONObject("user"));

                        saveLog(result, sessions);
                        return result.toString();
                    }
                    saveLog(result, sessions);
                    return result.toString();

                case "requestResetPasswordByAdmin":
                    result = ExternalServices.CheckTerminal(req.getJSONObject("terminal"));
                    if (result.getString("rc").equals("00"))
                    {
                        sessions.setBankId(result.getJSONObject("data").getLong("bank_id"));
                        result = AccountManagement.getOTP(req);
                        saveLog(Constants.RM_DATA_NOT_FOUND, sessions);
                        return result.toString();
                    }
                    saveLog(result, sessions);
                    return result.toString();

                case "getAllUserByBankId":
                    result = ExternalServices.CheckTerminal(req.getJSONObject("terminal"));
                    if (result.getString("rc").equals(Constants.RC_SUCCESS))
                    {
                        sessions.setBankId(result.getJSONObject("data").getLong("bank_id"));
                        return AccountManagement.getAllUserByBankId(req).toString();
                    }
                    return result.toString();

                case "activateUser":
                    result = ExternalServices.CheckTerminal(req.getJSONObject("terminal"));
                    if (result.getString("rc").equals(Constants.RC_SUCCESS))
                    {
                        sessions.setBankId(result.getJSONObject("data").getLong("bank_id"));
                        return AccountManagement.activateUser(req).toString();
                    }
                    return result.toString();

                case "deactivateUser":
                    result = ExternalServices.CheckTerminal(req.getJSONObject("terminal"));
                    if (result.getString("rc").equals(Constants.RC_SUCCESS))
                    {
                        sessions.setBankId(result.getJSONObject("data").getLong("bank_id"));
                        return AccountManagement.deactiveUser(req).toString();
                    }
                    return result.toString();

                case "getAdminByBankId":
                    result = Extras.getAdminByBankId(req);
                    saveLog(result, sessions);
                    return result.toString();

                case "getBankData":
                    result = Extras.getBankData(req);
                    saveLog(result, sessions);
                    return result.toString();

                case "getAllBankData":
                    result = Extras.getAllBankData();
                    saveLog(result, sessions);
                    return result.toString();

                case "sendEmail":
                    //GmailQuickstart.sendMessage()
                    return null;

                case "requestResetPasswordByUser":
                    result = AccountManagement.forgetPassword2(req);
                    saveLog(result, sessions);
                    return result.toString();

                case "changePassword":
                    result = AccountManagement.resetPassword(req);
                    saveLog(result, sessions);
                    return result.toString();
                case "checkAdminAvailability":
                    result = Extras.doesBankHaveAdmin(req);
                    saveLog(result, sessions);
                    return result.toString();
                case "sendNotifPenagihan":
                    result = AccountManagement.sendNotifPenagihan(req);
                    saveLog(result, sessions);
                    return result.toString();
                case "tesEmail":
                    result = AccountManagement.tesEMail(req.getString("email"));
                    saveLog(result, sessions);
                    return result.toString();
                case "addSerialNumber":
                    result = AddSerialNumber.addSerialNumber(req);
                    return result.toString();  
                case "updateSerialNumber":
                    result = AddSerialNumber.updateSerialNumber(req);
                    return result.toString();   
                case "addNewUser":
                    result = AddNewUser.addNewUser(req);
                    return result.toString();  
                case "updateUserData":
                    result = AddNewUser.updateUserData(req);
                    return result.toString();
                case "deleteSerialNumber":
                    result = AddSerialNumber.deleteSerialNumber(req);
                    return result.toString();
                case "getAllLembaga":
                    result = GetAllLembaga.getAllLembaga(req);
                    return result.toString();
                case "getStatusLoginLembaga":
                    result = GetAllLembaga.getStatusLoginLembaga(req);
                    return result.toString();
                case "updateStatusLoginLembaga":
                    result = GetAllLembaga.updateStatusLoginLembaga(req);
                    return result.toString();
                case "getBankId":
                    result = GetTerminal.getBankId(req);
                    return result.toString();
                default:
                    saveLog(Constants.RM_PATH_NOT_FOUND, sessions);
                    return new JSONObject().put("rc", Constants.RC_PATH_NOT_FOUND).put("rm", Constants.RM_PATH_NOT_FOUND).toString();
            }
            

        } catch (NoResultException e)
        {
            log.error(e.getMessage());
            sessions.setRc(Constants.RC_NOT_FOUND);
            sessions.setRm(Constants.RM_DATA_NOT_FOUND);
            sessions.setResBody("no result");
            sessions.setResTime(new Date());
            sessions.setException(e.getMessage());
            final Calendar timeEnd = Calendar.getInstance();
            sessions.setPerformanceTime(System.currentTimeMillis() - startTime);
            System.out.println("asd simpan");
            ///log.info("\n .::RESPONSE::. \n\n" + Constants.RM_DATA_NOT_FOUND);
            log.info(temp_log + "\n\n .::RESPONSE::. \n\n" + Constants.RM_DATA_NOT_FOUND + "\n\n");
            System.out.println("asd no resultexception " + e.getMessage());
            SInit.getSessionsDao().saveorupdate(sessions);
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND).toString();
        } catch (Exception e)
        {
            e.printStackTrace();
            log.info(e.getMessage());
            log.error(new String(), e);
            sessions.setRc(Constants.RC_IE);
            sessions.setException(e.getMessage());
            sessions.setRm(Constants.RM_IE);
            sessions.setResBody("Exception");
            sessions.setResTime(new Date());
            final Calendar timeEnd = Calendar.getInstance();
            sessions.setPerformanceTime(System.currentTimeMillis() - startTime);
            System.out.println("asd simpan");
            log.info(temp_log + "\n .::RESPONSE::. \n" + Constants.RM_IE + "\n\n");
            System.out.println("asd exception " + e.getMessage());
            SInit.getSessionsDao().saveorupdate(sessions);
            return internalServerError().toString();
        }
        //SInit.getSessionsDao().saveorupdate(sessions);

    }

    private void saveLog(JSONObject data, Sessions sessions) {
        log.info(temp_log + "\n .::RESPONSE::. \n" + data);
        if (data.has("rc"))
        {
            sessions.setRc(data.getString("rc"));
        }
        if (data.has("rm"))
        {
            sessions.setRm(data.getString("rm"));
        }
        sessions.setResBody(data.toString());
        sessions.setResTime(new Date());
        sessions.setPerformanceTime(System.currentTimeMillis() - startTime);
        SInit.getSessionsDao().saveorupdate(sessions);
        log.info("<== ending request section");
    }

    private void saveLog(String data, Sessions sessions) {
        log.info(temp_log + "\n .::RESPONSE::. \n" + data);
        sessions.setResBody(data);
        sessions.setResTime(new Date());
        sessions.setPerformanceTime(System.currentTimeMillis() - startTime);
        SInit.getSessionsDao().saveorupdate(sessions);
        log.info("<== ending request section");
    }

//    @RequestMapping(value = "/sb/extras/{path}", method = RequestMethod.POST)
//    public String getRequestMultipart(
//            @RequestPart(name = "file",required = false) MultipartFile zip,
//            @RequestPart(name = "body_request") String body,
//            @PathVariable("path") String path,
//            HttpServletRequest request) {
//
//        final JSONObject req = new JSONObject(body);
//        JSONObject jsonResponse;
//        final Calendar timeStart = Calendar.getInstance();
//        final Sessions sessions = new Sessions();
//        try {
//            //init session
//            sessions.setReqBody(body);
//            sessions.setPath(path);
//            sessions.setReqTime(new Date());
//            switch (path){
//                case "upload":
//                    jsonResponse = null
//                    return jsonResponse.toString();
//                case "download":
//                    return null;
//                default: return new JSONObject().put("rc", Constants.RC_PATH_NOT_FOUND).put("rm",Constants.RM_PATH_NOT_FOUND).toString();
//
//            }
//
//        } catch (Exception e) {
//            System.out.println("asd exception "+e.getMessage());
//            e.printStackTrace();
//            return internalServerError().toString();
//        }
//
//    }
}
