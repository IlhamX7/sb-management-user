package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.TripleDES;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Lembaga;
import com.dak.perbarindo.sbdbcontroller.gateway.model.ResponseMessage;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Terminal;
import com.dak.perbarindo.sbdbcontroller.gateway.model.UserApp;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserAuth {
    
    static final Logger log = LoggerFactory.getLogger("UserAuth");

    public static JSONObject CheckLogin(JSONObject req) throws Exception {

        final TripleDES tripleDES = new TripleDES(Constants.TDES_KEY_APP);
        final JSONObject reqBody = req.getJSONObject("user");
        final JSONObject terminalParam = req.getJSONObject("terminal");

        //fetch user data by username
        log.info("asd username "+reqBody.getString("username"));
        ArrayList<UserApp> users = (ArrayList<UserApp>)SInit.getUserAppDao().getEm().createQuery("select u from UserApp u where u.username=:pusername")
                .setParameter("pusername",reqBody.getString("username"))
                .getResultList();


        //does username exist
        if(users.isEmpty()){
            log.info("asd user null");
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("user_not_found");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(), responseMessage.getRm());
        }
        UserApp user = users.get(0);

        //does user blocked
        int maxLoginAttempt = Integer.parseInt(SInit.getConfigurationDao().getValueByName("max_login_attempt"));
        if(user.getFailLogin()==maxLoginAttempt){
            ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("max_login_attempt");
            return ResponseGenerator.ResponseGenerator(rm.getRc(),rm.getRm());
        }

        //Syncronize duedate
        Lembaga lembaga = SInit.getLembagaDao().findByBankId(user.getBankId().getBankId());
//        lembaga.setDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(req.getJSONObject("duedate").getString("due_date")));
//        SInit.getLembagaDao().saveorupdate(lembaga);

        //check due date
        log.info("Asd dude date "+user.getBankId().getDueDate());
        if (new SimpleDateFormat("yyyy-MM-dd").parse(user.getBankId().getDueDate().toString()).before(new Date())
                && !DateUtils.isSameDay(user.getBankId().getDueDate(),new Date())) {
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("due_date_passed");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        //check username and password
        if(!user.getPassword().equals(reqBody.getString("password"))){


            user.setFailLogin(user.getFailLogin()+1);
            if(user.getFailLogin()==5)user.setStatus(2);
            SInit.getUserAppDao().saveorupdate(user);
            //check attempt
            if(user.getFailLogin()==3){
                ResponseMessage rm = SInit.getResponseMessageDao().findByResponseName("max_login_attempt_warning");
                return ResponseGenerator.ResponseGenerator(rm.getRc(),rm.getRm());
            }
            log.info("asd password usernam incorret");
            final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("username_password_invalid");
            return ResponseGenerator.ResponseGenerator(responseMessage.getRc(), responseMessage.getRm());
        }


        if(user.getStatus()==0) {
             final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("user_inactivate");
             return ResponseGenerator.ResponseGenerator(responseMessage.getRc(),responseMessage.getRm());
        }

        final JSONObject dataUser = new JSONObject();
        dataUser.put("nik",tripleDES.decrypt(user.getNik()));
        dataUser.put("username",tripleDES.decrypt(user.getUsername()));
        dataUser.put("bank_name",user.getBankId().getBankName());
        dataUser.put("email",user.getEmail());
        dataUser.put("no_hp",user.getPhoneNumber());
        dataUser.put("expired_password",user.getExpiredPassword());
        dataUser.put("nama",user.getFullname());
        dataUser.put("due_date",user.getBankId().getDueDate());
        dataUser.put("privilage",user.getUserType());
        dataUser.put("id",user.getUserId());
        dataUser.put("due_date",req.getJSONObject("duedate"));
        if (user.getStatusLogin() == 1) {

            if (user.getTerminalLogin() == null) {
                final Date lastLogin = user.getLastLogin();
                final int dateDiff = Constants.getDateDifferences(new Date(), lastLogin);
//                log.info("asd date diff : "+dateDiff);
//                if (dateDiff == 0) {
                    final Terminal terminal = SInit.getTerminalDao().findById(req.getInt("terminal_id"));
                    user.setTerminalLogin(terminal);
                    user.setLastLogin(new Date());
                    final Lembaga lembaga1 = user.getBankId();
                    lembaga1.setLastLoginTime(new Date());
                    SInit.getLembagaDao().saveorupdate(lembaga1);
                    user.setFailLogin(0);
                    SInit.getUserAppDao().saveorupdate(user);
                    return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS,dataUser);
//                }
//                final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("login_failed");
//                return ResponseGenerator.ResponseGenerator(responseMessage.getRc(), responseMessage.getRm());
            }

            if (user.getTerminalLogin().getTerminalId() == req.getLong("terminal_id")) {
                user.setLastLogin(new Date());
                final Lembaga lembaga1 = user.getBankId();
                lembaga1.setLastLoginTime(new Date());
                SInit.getLembagaDao().saveorupdate(lembaga1);
                user.setFailLogin(0);
                SInit.getUserAppDao().saveorupdate(user);
                return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS,dataUser);
            } else {
                final ResponseMessage responseMessage = SInit.getResponseMessageDao().findByResponseName("different_terminal_login");
                user.setStatusLogin(0);
                user.setTerminalLogin(null);
                SInit.getUserAppDao().saveorupdate(user);
                return ResponseGenerator.ResponseGenerator(responseMessage.getRc(), responseMessage.getRm());
            }
        } else {

            log.info("asd nih "+req.getInt("terminal_id"));
            final Terminal terminal = SInit.getTerminalDao().findById(req.getInt("terminal_id"));
            final Lembaga lembaga1 = user.getBankId();
            lembaga1.setLastLoginTime(new Date());
            SInit.getLembagaDao().saveorupdate(lembaga1);
            user.setStatusLogin(1);
            user.setLastLogin(new Date());
            user.setFailLogin(0);
            log.info("asd nih "+terminalParam.getString("mac"));
            if(terminalParam.has("mac")) terminal.setMacAddress(terminalParam.getString("mac"));
            log.info("asd nih 2222");
            if(terminalParam.has("uuid")) terminal.setUuid(terminalParam.getString("uuid"));
            if(terminalParam.has("hostname"))terminal.setHostName(terminalParam.getString("hostname"));
            if(terminalParam.has("proc_id"))terminal.setProcessorId(terminalParam.getString("proc_id"));
            if(terminalParam.has("proc_name"))terminal.setProcessorName(terminalParam.getString("proc_name"));
            if(terminalParam.has("win_serial"))terminal.setSerialNumber(terminalParam.getString("win_serial"));
            SInit.getTerminalDao().saveorupdate(terminal);
            user.setTerminalLogin(terminal);
            SInit.getUserAppDao().saveorupdate(user);
            return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS,dataUser);
        }
    }

    public static JSONObject logout(JSONObject reqBody) {
        UserApp UserApp = (UserApp) SInit.getUserAppDao().getEm().createQuery("select u from UserApp u where u.username=:pusername")
                .setParameter("pusername", reqBody.getString("username"))
                .getSingleResult();
        UserApp.setStatusLogin(0);
        UserApp.setTerminalLogin(null);
        SInit.getUserAppDao().saveorupdate(UserApp);
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS,new JSONObject().put("id",UserApp.getUserId()));
    }
}
