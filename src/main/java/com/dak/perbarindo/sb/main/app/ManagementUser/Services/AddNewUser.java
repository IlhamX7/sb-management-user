/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sbdbcontroller.gateway.model.UserApp;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.validator.routines.EmailValidator;

/**
 *
 * @author ilham
 */
public class AddNewUser {

    static final Logger log = LoggerFactory.getLogger("AddNewUser");

    public static JSONObject addNewUser(JSONObject reqBody) {
        JSONObject response = new JSONObject();

        String cr_time = reqBody.getString("cr_time");
        String username = reqBody.getString("username");
        String password = reqBody.getString("password");
        String fullname = reqBody.getString("fullname");
        String nik = reqBody.getString("nik");
        String email = reqBody.getString("email");
        String phone_number = reqBody.getString("phone_number");
        Integer user_type = reqBody.getInt("user_type");
        String expired_password = reqBody.getString("expired_password");
        Integer fail_login = reqBody.getInt("fail_login");
        Integer status = reqBody.getInt("status");
        String info = reqBody.getString("info");
        String additional_data = reqBody.getString("additional_data");
        Integer status_login = reqBody.getInt("status_login");
        String last_login = reqBody.getString("last_login");
        String last_logout = reqBody.getString("last_logout");
        String activate_date = reqBody.getString("activate_date");

        Date crTime = null;
        try {
            crTime = new SimpleDateFormat("dd/MM/yyyy").parse(cr_time);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date expiredPassword = null;
        try {
            expiredPassword = new SimpleDateFormat("dd/MM/yyyy").parse(expired_password);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date lastLogin = null;
        try {
            lastLogin = new SimpleDateFormat("dd/MM/yyyy").parse(last_login);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date lastLogout = null;
        try {
            lastLogout = new SimpleDateFormat("dd/MM/yyyy").parse(last_logout);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date activateDate = null;
        try {
            activateDate = new SimpleDateFormat("dd/MM/yyyy").parse(activate_date);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        UserApp checkUsername = SInit.getUserAppDao().login(username);
        EmailValidator validator = EmailValidator.getInstance();

        UserApp userApp = new UserApp();

        if (checkUsername == null && validator.isValid(email)) {
            userApp.setCrTime(crTime);
            userApp.setUsername(username);
            userApp.setPassword(password);
            userApp.setFullname(fullname);
            userApp.setNik(nik);
            userApp.setEmail(email);
            userApp.setPhoneNumber(phone_number);
            userApp.setUserType(user_type);
            userApp.setExpiredPassword(expiredPassword);
            userApp.setFailLogin(fail_login);
            userApp.setStatus(status);
            userApp.setInfo(info);
            userApp.setAdditionalData(additional_data);
            userApp.setStatusLogin(status_login);
            userApp.setLastLogin(lastLogin);
            userApp.setLastLogout(lastLogout);
            userApp.setActivateDate(activateDate);

            SInit.getUserAppDao().saveorupdate(userApp);

            response.put("rc", "00");
            response.put("rm", "Success");
            response.put("crTime", crTime);
            response.put("username", username);
            response.put("password", password);
            response.put("fullname", fullname);
            response.put("nik", nik);
            response.put("email", email);
            response.put("phone_number", phone_number);
            response.put("user_type", user_type);
            response.put("expiredPassword", expiredPassword);
            response.put("fail_login", fail_login);
            response.put("status", status);
            response.put("info", info);
            response.put("additional_data", additional_data);
            response.put("status_login", status_login);
            response.put("lastLogin", lastLogin);
            response.put("lastLogout", lastLogout);
            response.put("activateDate", activateDate);

            return response;
        } else if (checkUsername != null && validator.isValid(email)) {
            response.put("rc", "01");
            response.put("rm", "username sudah ada");
            return response;
        } else if (checkUsername == null && !validator.isValid(email)) {
            response.put("rc", "01");
            response.put("rm", "email salah format");
            return response;
        } else {
            response.put("rc", "01");
            response.put("rm", "username sudah ada dan email salah format");
        }
        return response;
    }

    public static JSONObject updateUserData(JSONObject reqBody) {

        Integer user_id = reqBody.getInt("user_id");
        String cr_time = reqBody.getString("cr_time");
        String username = reqBody.getString("username");
        String password = reqBody.getString("password");
        String fullname = reqBody.getString("fullname");
        String nik = reqBody.getString("nik");
        String email = reqBody.getString("email");
        String phone_number = reqBody.getString("phone_number");
        Integer user_type = reqBody.getInt("user_type");
        String expired_password = reqBody.getString("expired_password");
        Integer fail_login = reqBody.getInt("fail_login");
        Integer status = reqBody.getInt("status");
        String info = reqBody.getString("info");
        String additional_data = reqBody.getString("additional_data");
        Integer status_login = reqBody.getInt("status_login");
        String last_login = reqBody.getString("last_login");
        String last_logout = reqBody.getString("last_logout");
        String activate_date = reqBody.getString("activate_date");

        Date crTime = null;
        try {
            crTime = new SimpleDateFormat("dd/MM/yyyy").parse(cr_time);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date expiredPassword = null;
        try {
            expiredPassword = new SimpleDateFormat("dd/MM/yyyy").parse(expired_password);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date lastLogin = null;
        try {
            lastLogin = new SimpleDateFormat("dd/MM/yyyy").parse(last_login);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date lastLogout = null;
        try {
            lastLogout = new SimpleDateFormat("dd/MM/yyyy").parse(last_logout);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        Date activateDate = null;
        try {
            activateDate = new SimpleDateFormat("dd/MM/yyyy").parse(activate_date);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddNewUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        JSONObject response = new JSONObject();

        UserApp userApp = SInit.getUserAppDao().findUserById(user_id);

        userApp.setCrTime(crTime);
        userApp.setUsername(username);
        userApp.setPassword(password);
        userApp.setFullname(fullname);
        userApp.setNik(nik);
        userApp.setEmail(email);
        userApp.setPhoneNumber(phone_number);
        userApp.setUserType(user_type);
        userApp.setExpiredPassword(expiredPassword);
        userApp.setFailLogin(fail_login);
        userApp.setStatus(status);
        userApp.setInfo(info);
        userApp.setAdditionalData(additional_data);
        userApp.setStatusLogin(status_login);
        userApp.setLastLogin(lastLogin);
        userApp.setLastLogout(lastLogout);
        userApp.setActivateDate(activateDate);

        SInit.getUserAppDao().saveorupdate(userApp);

        response.put("rc", "00");
        response.put("rm", "Success");
        response.put("crTime", crTime);
        response.put("username", username);
        response.put("password", password);
        response.put("fullname", fullname);
        response.put("nik", nik);
        response.put("email", email);
        response.put("phone_number", phone_number);
        response.put("user_type", user_type);
        response.put("expiredPassword", expiredPassword);
        response.put("fail_login", fail_login);
        response.put("status", status);
        response.put("info", info);
        response.put("additional_data", additional_data);
        response.put("status_login", status_login);
        response.put("lastLogin", lastLogin);
        response.put("lastLogout", lastLogout);
        response.put("activateDate", activateDate);

        return response;
    }
}
