package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sbdbcontroller.gateway.model.AppSerialNumber;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Configuration;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Lembaga;
import com.dak.perbarindo.sbdbcontroller.gateway.model.UserApp;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.persistence.NoResultException;
import java.util.ArrayList;


public class Extras {

    public static JSONObject getAdminByBankId(JSONObject jsonBody) throws NoResultException {
        UserApp user = (UserApp) SInit.getUserAppDao().getEm().createQuery("select u from UserApp u where u.Lembaga.bankId=:pid and u.privilege=1")
                .setParameter("pid", jsonBody.getInt("bank_id"))
                .getSingleResult();
        JSONObject jsonObject = new JSONObject();
        if (user.getActivateDate() != null)
            jsonObject.put("active_date", user.getActivateDate());
        if (user.getCrTime() != null)
            jsonObject.put("dtm_created", user.getCrTime());
        if (user.getModTime() != null)
            jsonObject.put("dtm_updated", user.getModTime());
        if (user.getEmail() != null)
            jsonObject.put("email", user.getEmail());
        if (user.getExpiredPassword() != null)
            jsonObject.put("expired_date", user.getExpiredPassword());
        if (user.getLastLogin() != null)
            jsonObject.put("last_login", user.getLastLogin());
        if (user.getLastLogout() != null)
            jsonObject.put("last_logout", user.getLastLogout());
        if (user.getFullname() != null)
            jsonObject.put("name", user.getFullname());
        if (user.getNik() != null)
            jsonObject.put("nik", user.getNik());
        if (user.getPhoneNumber() != null)
            jsonObject.put("no_hp", user.getPhoneNumber());
        if (user.getPassword() != null)
            jsonObject.put("password", user.getPassword());
        if (user.getUserType() != null)
            jsonObject.put("privillege", user.getUserType());
        if (user.getStatus() != null)
            jsonObject.put("status", user.getStatus());
        if (user.getStatusLogin() != null)
            jsonObject.put("status_login", user.getStatusLogin());
        if (user.getUsername() != null)
            jsonObject.put("username", user.getUsername());
        if (user.getTerminalLogin() != null)
            jsonObject.put("id_terminal", user.getTerminalLogin());
        if (user.getBankId() != null)
            jsonObject.put("bank_id", user.getBankId().getBankId());
        if (user.getFailLogin() != null)
            jsonObject.put("fail_login", user.getFailLogin());
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, jsonObject);
    }

    public static JSONObject getBankData(JSONObject reqBody) throws NoResultException {
        Lembaga lembaga = SInit.getLembagaDao().findByBankId(reqBody.getLong("bank_id"));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("bank_id", lembaga.getBankId());
        jsonObject.put("bank_name", lembaga.getBankName());
        if(lembaga.getCrTime()!=null)
        jsonObject.put("cr_time", lembaga.getCrTime());
        if(lembaga.getAdditionalData()!=null)
        jsonObject.put("additional_data", lembaga.getAdditionalData());
        if(lembaga.getInfo()!=null)
        jsonObject.put("info",lembaga.getInfo());
        if(lembaga.getModTime()!=null)
        jsonObject.put("mod_time", lembaga.getModTime());
        jsonObject.put("status", lembaga.getStatus());
        jsonObject.put("terminal_name", lembaga.getBankName());
        jsonObject.put("duedate_time", lembaga.getDueDate());
        jsonObject.put("last_login_time", lembaga.getLastLoginTime());
        jsonObject.put("status_login", lembaga.getStatusLogin());
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, jsonObject);
    }

    public static JSONObject getAllBankData() throws NoResultException {
        ArrayList<Lembaga> Lembagas = (ArrayList<Lembaga>) SInit.getLembagaDao().getEm().createQuery("select m from Lembaga m ").getResultList();
        JSONObject jsonObject;
        JSONArray jsonArray = new JSONArray();
        for (Lembaga lembaga : Lembagas) {
            jsonObject = new JSONObject();
            jsonObject.put("bank_id", lembaga.getBankId());
            jsonObject.put("bank_name", lembaga.getBankName());
            if(lembaga.getCrTime()!=null)
                jsonObject.put("cr_time", lembaga.getCrTime());
            if(lembaga.getAdditionalData()!=null)
                jsonObject.put("additional_data", lembaga.getAdditionalData());
            if(lembaga.getInfo()!=null)
                jsonObject.put("info",lembaga.getInfo());
            if(lembaga.getModTime()!=null)
                jsonObject.put("mod_time", lembaga.getModTime());
            jsonObject.put("status", lembaga.getStatus());
            jsonObject.put("terminal_name", lembaga.getBankName());
            jsonObject.put("duedate_time", lembaga.getDueDate());
            jsonObject.put("last_login_time", lembaga.getLastLoginTime());
            jsonObject.put("status_login", lembaga.getStatusLogin());
            jsonArray.put(jsonObject);
        }
        return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, jsonArray);
    }

    public static JSONObject doesBankHaveAdmin(JSONObject reqBody) throws Exception{
        try {
            final ArrayList<UserApp> user = (ArrayList<UserApp>) SInit.getUserAppDao().getEm().createQuery("select u from UserApp u where u.bankId.bankId=:pbankid and u.userType=1")
                    .setParameter("pbankid", reqBody.getLong("bank_id"))
                    .getResultList();
            if(user.size()!=0)
            return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS);
            final Configuration configuration = SInit.getConfigurationDao().findByConfigName("disclaimer_create_admin");
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND,configuration.getValue());
        }catch (Exception e){
           throw new Exception();
        }

    }





}
