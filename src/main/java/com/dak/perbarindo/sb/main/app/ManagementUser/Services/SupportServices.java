/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import static com.dak.perbarindo.sb.main.app.ManagementUser.Services.ExternalServices.log;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ROG
 */
public class SupportServices {

    static final Logger log = LoggerFactory.getLogger("support_services");

    public static JSONObject broadcastResetPassword(String toemail, String toname, String username, String password) {
        // tag spsrv-brp
        try
        {
            JSONObject jsonReqBody = new JSONObject()
                    .put("toemail", toemail)
                    .put("toname", toname)
                    .put("username", username)
                    .put("password", password);

            log.info("send reqBody: " + jsonReqBody.toString());
            log.info("send to broadcastResetPassword");

            HttpPost httpPost = new HttpPost("http://172.16.50.7:16011/service/broadcast/sendemail/resetpassword");
            httpPost.setEntity(new StringEntity(jsonReqBody.toString()));
            httpPost.setHeader("Content-Type", "application/json");
            JSONObject response = sendRequest(httpPost);
            log.info("get result: " + response.toString());
            return response;
        } catch (Exception e)
        {
            log.error(new String(), e);
            return new JSONObject().put("rc", "99").put("rm", "Terjadi Kesalahan [spsrv-brp11]");
        }
    }

    public static JSONObject sendRequest(HttpPost httpPost) {
        try
        {
            final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse response = httpClient.execute(httpPost);
            final BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
            String line;
            final StringBuilder respMsg = new StringBuilder();
            while ((line = br.readLine()) != null)
            {
                respMsg.append(line);
            }
            final JSONObject jsonResponseAPI = new JSONObject(respMsg.toString());
            return jsonResponseAPI;
        } catch (Exception e)
        {
            log.error(new String(), e);
            return new JSONObject().put("rc", "99").put("rm", "Terjadi Kesalahan [spsrv-brp12]");
        }
    }

    public static JSONObject supportSenqInquiryInvoice(String invoice) {
        try
        {
            String urlInquiry = SInit.getConfigurationDao().getValueByName("url_inquiry");
            if (urlInquiry == null)
            {
                urlInquiry = Constants.URL_INQUIRY;
            }
            String header = SInit.getConfigurationDao().getValueByName("header_service_ari");
            if (header == null)
            {
                header = Constants.HEADER_SERVICE_ARI;
            }
            log.info("urlinquiry: " + urlInquiry);

            JSONObject jsonReqBody = new JSONObject()
                    .put("invoice_number", invoice);

            log.info("send reqBody: " + jsonReqBody.toString());
            log.info("send to supportSenqInquiryInvoice");

            HttpPost httpPost = new HttpPost(urlInquiry);
            httpPost.setEntity(new StringEntity(jsonReqBody.toString()));
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", header);
            JSONObject response = sendRequest(httpPost);
            log.info("response: " + response.toString());
            
            if (!response.has("rc")){
                return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, new JSONObject(response));
            } else {
                return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
            }
        } catch (Exception e)
        {
            log.error(new String(), e);
            return null;
        }
    }
    
    public static JSONObject supportGetInvoice(JSONObject reqbody) {
        try
        {
            String urlInquiry = SInit.getConfigurationDao().getValueByName("url_get_invoice");
            if (urlInquiry == null)
            {
                urlInquiry = "https://lumen.srv.co.id/api/sb/invoice";
            }
            String header = SInit.getConfigurationDao().getValueByName("header_service_ari");
            if (header == null)
            {
                header = Constants.HEADER_SERVICE_ARI;
            }
            log.info("urlinquiry: " + urlInquiry);

            JSONObject jsonReqBody = new JSONObject()
                    .put("bank_id", String.valueOf(reqbody.getLong("bank_id")))
                    .put("status", "unpaid");

            log.info("send reqBody: " + jsonReqBody.toString());
            log.info("send to supportGetInvoice");

            HttpPost httpPost = new HttpPost(urlInquiry);
            httpPost.setEntity(new StringEntity(jsonReqBody.toString()));
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", header);
            JSONObject response = sendRequest(httpPost);
            log.info("response: " + response.toString());
            
            if (!response.has("rc")){
                return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, new JSONObject(response));
            } else {
                return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
            }
        } catch (Exception e)
        {
            log.error(new String(), e);
            return null;
        }
    }
}
