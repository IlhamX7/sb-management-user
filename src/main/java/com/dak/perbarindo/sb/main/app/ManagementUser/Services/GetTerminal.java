/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sbdbcontroller.gateway.model.Terminal;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import java.util.List;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ilham
 */
public class GetTerminal {
    
    static final Logger log = LoggerFactory.getLogger("GetTerminal");
    
    public static JSONObject getBankId(JSONObject reqBody) {
        Integer terminal_id = reqBody.getInt("terminal_id");
        
        JSONObject response = new JSONObject();
        
        List<Terminal> terminalData = SInit.getTerminalDao().getById(terminal_id);
        
        return response.put("data", terminalData);
//        return response.put("data", SInit.getTerminalDao().getTerminalLastId(terminal_id));
    }
}
