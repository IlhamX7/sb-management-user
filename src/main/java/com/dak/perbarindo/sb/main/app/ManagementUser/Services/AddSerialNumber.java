/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sbdbcontroller.gateway.model.AppSerialNumber;
import static com.dak.perbarindo.sbdbcontroller.gateway.model.AppSerialNumber_.serialNumber;
import static com.dak.perbarindo.sbdbcontroller.gateway.model.AppVersion_.id;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import java.text.ParseException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author ilham
 */
public class AddSerialNumber {

    static final Logger log = LoggerFactory.getLogger("AddSerialNumber");

    public static JSONObject addSerialNumber(JSONObject reqBody) {
        Date generateNumber = null;
        Date usedTime = null;
        String serial_number = reqBody.getString("serial_number");
        String generate_number = reqBody.getString("generate_number");

        try {
            generateNumber = new SimpleDateFormat("dd/MM/yyyy").parse(generate_number);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddSerialNumber.class.getName()).log(Level.SEVERE, null, ex);
        }
        String used_time = reqBody.getString("used_time");

        try {
            usedTime = new SimpleDateFormat("dd/MM/yyyy").parse(used_time);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddSerialNumber.class.getName()).log(Level.SEVERE, null, ex);
        }
        String mac_address = reqBody.getString("mac_address");
        Integer status = reqBody.getInt("status");
        String info = reqBody.getString("info");
        String additional_data = reqBody.getString("additional_data");

        JSONObject response = new JSONObject();

        AppSerialNumber SerialNumber = new AppSerialNumber();

        SerialNumber.setSerialNumber(serial_number);
        SerialNumber.setGenerateNumber(generateNumber);
        SerialNumber.setUsedTime(usedTime);
        SerialNumber.setMacAddress(mac_address);
        SerialNumber.setStatus(status);
        SerialNumber.setInfo(info);
        SerialNumber.setAdditionalData(additional_data);
        SInit.getAppSerialNumberDao().saveorupdate(SerialNumber);

        response.put("rc", "00");
        response.put("rm", "Success");
        response.put("serial_number", serial_number);
        response.put("generateNumber", generateNumber);
        response.put("usedTime", usedTime);
        response.put("mac_address", mac_address);
        response.put("status", status);
        response.put("info", info);
        response.put("additional_data", additional_data);
        return response;
    }

    public static JSONObject updateSerialNumber(JSONObject reqBody) {

        String serial_number = reqBody.getString("serial_number");
        String generate_number = reqBody.getString("generate_number");
        String used_time = reqBody.getString("used_time");
        String mac_address = reqBody.getString("mac_address");
        Integer status = reqBody.getInt("status");
        String info = reqBody.getString("info");
        String additional_data = reqBody.getString("additional_data");

        JSONObject response = new JSONObject();

        Date generateNumber = null;
        try {
            generateNumber = new SimpleDateFormat("dd/MM/yyyy").parse(generate_number);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddSerialNumber.class.getName()).log(Level.SEVERE, null, ex);
        }

        Date usedTime = null;
        try {
            usedTime = new SimpleDateFormat("dd/MM/yyyy").parse(used_time);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(AddSerialNumber.class.getName()).log(Level.SEVERE, null, ex);
        }

        AppSerialNumber appSerialNumber = SInit.getAppSerialNumberDao().findBySN(serial_number);

        appSerialNumber.setGenerateNumber(generateNumber);
        appSerialNumber.setUsedTime(usedTime);
        appSerialNumber.setMacAddress(mac_address);
        appSerialNumber.setStatus(status);
        appSerialNumber.setInfo(info);
        appSerialNumber.setAdditionalData(additional_data);

        SInit.getAppSerialNumberDao().saveorupdate(appSerialNumber);

        response.put("rc", "00");
        response.put("rm", "Success");
        response.put("serial_number", serial_number);
        response.put("generate_number", generateNumber);
        response.put("used_time", usedTime);
        response.put("mac_address", mac_address);
        response.put("status", status);
        response.put("info", info);
        response.put("addtional_data", additional_data);
        return response;
    }

    public static JSONObject deleteSerialNumber(JSONObject reqBody) {

        String serial_number = reqBody.getString("serial_number");

        AppSerialNumber appSerialNumber = SInit.getAppSerialNumberDao().findBySN(serial_number);

        JSONObject response = new JSONObject();

        SInit.getAppSerialNumberDao().deleteSerialNumber(serial_number);

        response.put("rc", "00");
        response.put("rm", "Success");
        response.put("serial_number", serial_number);

        return response;
    }
}
