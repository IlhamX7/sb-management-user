package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sb.main.app.ManagementUser.Models.ResponseGenerator;
import com.dak.perbarindo.sb.main.app.ManagementUser.Utils.Constants;
import com.dak.perbarindo.sbdbcontroller.gateway.dao.UserAppDao;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Configuration;
import com.dak.perbarindo.sbdbcontroller.gateway.model.Lembaga;
import com.dak.perbarindo.sbdbcontroller.gateway.model.UserApp;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.catalina.User;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.*;

public class ExternalServices {

    static final Logger log = LoggerFactory.getLogger("ExternalServices");

    public static JSONObject CheckTerminal(JSONObject reqBody) {
        //1.Melakukan http request ke service management application untuk cek terminal
        //String urlCheckTerminal = SInit.getConfigurationDao().getValueByName("url_check_terminal_status_dev");
//        String urlCheckTerminal = "http://103.28.148.203:61004/sb/checkTerminalStatus";
        String urlCheckTerminal = "http://localhost:16004/sb/checkTerminalStatus";
//        if (urlCheckTerminal == null) {
//            urlCheckTerminal = Constants.URL_CHECK_STATUS;
//        }
//
        final RestTemplate restTemplate = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        final Map<String, Object> map = new HashMap<>();
        map.put("mac", reqBody.getString("mac"));
        map.put("proc_name", reqBody.getString("proc_name"));
        map.put("proc_id", reqBody.getString("proc_id"));
        map.put("uuid", reqBody.getString("uuid"));
        map.put("version", "3.1.44");
        if (reqBody.has("bank_id")) {
            map.put("bank_id", reqBody.getInt("bank_id"));
        }
        final HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        final ResponseEntity<String> response = restTemplate.postForEntity(urlCheckTerminal, entity, String.class);
        System.out.println("asd response asli " + response.getBody());
        log.info("get response " + response.getBody());
        if (response.getStatusCodeValue() == 200) {
            return new JSONObject(response.getBody());
        } else {
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
        }
    }

    public static String forward(JSONObject reqBody) throws Exception {
        final String url = SInit.getConfigurationDao().getValueByName("url_forward");
        final OkHttpClient client = new OkHttpClient();
        okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/json");
        final RequestBody body = RequestBody.create(mediaType, reqBody.toString());
        final Request request = new Request.Builder()
                .url(url)
                .post(body)
                .addHeader("Content-Type", "application/json")
                .build();
        return client.newCall(request).execute().body().string();
    }

    public static JSONObject checkDuedate(JSONObject reqBody) {

        if (reqBody.getString("bank_id").equals("2499")) {
            return new JSONObject(SInit.getConfigurationDao().getValueByName("developer_dummy_checkduedate"));
//            return new JSONObject("{\n" +
//                    "  \"rc\": \"00\",\n" +
//                    "  \"rm\": {\n" +
//                    "    \"due_date\": \"2022-12-20 00:00:00.0\",\n" +
//                    "    \"rm\": \"Tagihan Tidak Ada\"\n" +
//                    "  }\n" +
//                    "}");
        }
        String urlCheckTerminal = SInit.getConfigurationDao().getValueByName("url_check_duedate");
        if (urlCheckTerminal == null) {
            urlCheckTerminal = Constants.URL_CHECK_STATUS;
        }
        final RestTemplate restTemplate = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        final Map<String, Object> map = new HashMap<>();
        map.put("bpr", reqBody.getString("bank_id"));
        final HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        final ResponseEntity<String> response = restTemplate.postForEntity(urlCheckTerminal, entity, String.class);
        System.out.println("asd response asli " + response.getBody());
        log.info("get response asli " + response.getBody());
        if (response.getStatusCodeValue() == 200) {
            return new JSONObject(response.getBody());
        } else {
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
        }
    }

    public static JSONObject sendInquiry(String invoice) {
        String urlInquiry = SInit.getConfigurationDao().getValueByName("url_inquiry");
        if (urlInquiry == null) {
            urlInquiry = Constants.URL_INQUIRY;
        }
        String header = SInit.getConfigurationDao().getValueByName("header_service_ari");
        if (header == null) {
            header = Constants.HEADER_SERVICE_ARI;
        }
        log.info("urlinquiry: " + urlInquiry);
        final RestTemplate restTemplate = new RestTemplate();
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", header);

        final Map<String, Object> map = new HashMap<>();
        map.put("invoice_number", invoice);
        final HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
        final ResponseEntity<String> response = restTemplate.postForEntity(urlInquiry, entity, String.class);
        System.out.println("asd response asli send inquiry " + response.getBody());
        log.info("get response asli send inquiry " + response.getBody());
        if (response.getStatusCodeValue() == 200) {
            return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, Constants.RM_SUCCESS, new JSONObject(response.getBody()));
        } else {
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
        }
    }

    public static void sendWAInvoice(String invoice, String phone, Logger log) {
        try {
            String urlSendInvoice = SInit.getConfigurationDao().getValueByName("send_wa_invoice");
            if (urlSendInvoice == null) {
                urlSendInvoice = Constants.URL_INQUIRY;
            }
            String header = SInit.getConfigurationDao().getValueByName("header_service_ari");
            if (header == null) {
                header = Constants.HEADER_SERVICE_ARI;
            }
            final RestTemplate restTemplate = new RestTemplate();
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.set("Authorization", header);
            final Map<String, Object> map = new HashMap<>();
            map.put("invoice_number", invoice);
            map.put("no_wa", phone);
            final HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
            final ResponseEntity<String> response = restTemplate.postForEntity(urlSendInvoice, entity, String.class);
            log.info("SENDING WA INVOICE RESPONSE " + response.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject checkInvoice(JSONObject reqBody) throws Exception {
//        final Logger log = LoggerFactory.getLogger("Extras");
        String urlGetInvoice = SInit.getConfigurationDao().getValueByName("array_reminder");
        final String[] arrayReminder = urlGetInvoice.split(",");
        final SimpleDateFormat sdf = new SimpleDateFormat("dd");
        final String currentDate = sdf.format(new Date());
        System.out.println("asd current date " + currentDate);
        log.info("get current date " + currentDate);
        final JSONObject responseGetInvoce;
        final boolean duedatePassed = new SimpleDateFormat("yyyy-MM-dd").parse(reqBody.getString("duedate")).before(new Date());
        System.out.println("asd duedate passed " + duedatePassed);
        System.out.println("asd dudate asli " + reqBody.getString("duedate"));
        log.info("get duedate passed " + duedatePassed);
        log.info("get dudate asli " + reqBody.getString("duedate"));
        if (Arrays.asList(arrayReminder).contains(currentDate) || duedatePassed) {
            log.info("GETTING INVOICE..");
//            responseGetInvoce = getInvoice(reqBody);
            responseGetInvoce = new JSONObject("{\n"
                    + "	\"rc\":\"" + Constants.RC_NOT_FOUND + "\",\n"
                    + "	\"rm\":\"Invoice Belum Bisa Di Generate\",\n"
                    + "	\"message\":\"Invoice Belum Bisa Di Generate\"\n"
                    + "}");
            
//    responseGetInvoce.put("message", "a");
//            responseGetInvoce.put("rc", Constants.RC_NOT_FOUND);
//            responseGetInvoce.put("rm","Invoice Belum Bisa Di Generate");
        } else {
            log.info("NOT DUE DATE OR REMINDER DATE");
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, Constants.RM_DATA_NOT_FOUND);
        }
        
        log.info("INVOICE " + responseGetInvoce.toString());
        
        if (responseGetInvoce.getString("message").equals("Berhasil get invoice")) {
            final JSONArray array = responseGetInvoce.getJSONArray("data");
            if (array.length() == 0) {
                return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Invoice not found");
            }
            final JSONObject invoice = array.getJSONObject(array.length() - 1);
            if (invoice.isNull("payment_code")) {
                System.out.println("asd payment code null, sending inquiery..");
                log.info("payment code null, sending inquiery..");
                final JSONObject responseSendInq = sendInquiry(invoice.getString("invoice_number"));
                if (responseSendInq.getString("rc").equals(Constants.RC_NOT_FOUND)) {
                    return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Send inquiry failed");
                }
            }

            //jika sudah melebihi due date maka kirim pemberitahuan wa
            if (duedatePassed) {
                Object phone = SInit.getUserAppDao().getAtasanPhoneNumber(reqBody.getLong("bank_id"));
                if (phone == null) {
                    return ResponseGenerator.ResponseGenerator(Constants.RC_IE, "Unable to get phone number");
                }
                String phoneNumber = phone.toString();
                phoneNumber = phoneNumber.replace("+", "");
                if (String.valueOf(phoneNumber.charAt(0)).equals("0")) {
                    phoneNumber = "62" + phoneNumber.substring(1);
                }
                log.info("Sending WA NOTIF : " + phoneNumber);
                sendWAInvoice(invoice.getString("invoice_number"), phoneNumber, log);
            }

            String conf = SInit.getConfigurationDao().getValueByName("invoice_format");
            conf = conf.replace("[TOTAL_AMOUNT]", invoice.getString("total"));
            conf = conf.replace("[DUE_DATE]", reqBody.getString("duedate"));
            return ResponseGenerator.ResponseGenerator(Constants.RC_SUCCESS, conf);
        } else if (responseGetInvoce.getString("rm").equals("Invoice Belum Bisa Di Generate")) {

            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Invoice Belum Bisa Di Generate");

        } else {
            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Invoice not found");
        }

    }

    public static JSONObject getInvoice(JSONObject reqBody) {
        String urlGetInvoice = SInit.getConfigurationDao().getValueByName("url_get_invoice");

        try {
            if (urlGetInvoice == null) {
                urlGetInvoice = Constants.URL_GET_INVOICE;
            }
            System.out.println("asd sout " + urlGetInvoice);
            log.info("get urlGetInvoice " + urlGetInvoice);
            String header = SInit.getConfigurationDao().getValueByName("header_service_ari");
            if (header == null) {
                header = Constants.HEADER_SERVICE_ARI;
            }
            final RestTemplate restTemplate = new RestTemplate();
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            headers.set("Authorization", header);
            final Map<String, Object> map = new HashMap<>();
            map.put("bank_id", String.valueOf(reqBody.getLong("bank_id")));
            map.put("status", "unpaid");
            final HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
            final ResponseEntity<String> response = restTemplate.postForEntity(urlGetInvoice, entity, String.class);
            System.out.println("asd response asli " + response.getBody());
            log.info("response asli " + response.getBody());
            if (response.getStatusCodeValue() == 200) {
                return new JSONObject(response.getBody());
            } else {
                return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Invoice not found");
            }
        } catch (Exception e) {

            String responeError = "{\n"
                    + "	\"rc\":\"" + Constants.RC_NOT_FOUND + "\",\n"
                    + "	\"rm\":\"Invoice Belum Bisa Di Generate\",\n"
                    + "	\"message\":\"Invoice Belum Bisa Di Generate\"\n"
                    + "}";
//            return ResponseGenerator.ResponseGenerator(Constants.RC_NOT_FOUND, "Invoice Belum Bisa Di Generate");
            return new JSONObject(responeError);
        }
    }
}
