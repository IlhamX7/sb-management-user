package com.dak.perbarindo.sb.main.app.ManagementUser.Utils;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.json.JSONObject;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {
    public static final String RC_SUCCESS = "00";
    public static final String RC_IE = "99";
    public static final String RC_NOT_FOUND = "04";
    public static final String RC_UNIQUE_CONSTRAINT = "05";
    public static final String RM_SUCCESS = "Success";
    public static final String RM_IE = "Oops, something went wrong :(";
    public static final String RM_DATA_NOT_FOUND = "Not found | Empty";
    public static final String RC_PATH_NOT_FOUND = "03";
    public static final String RM_PATH_NOT_FOUND = "Service path not found";
    public static final String TDES_KEY_APP = "APLIKASISHARINGBANDWIDTHENKRIPSI";
    public static final String APPLICATION_NAME = "Gmail API Java Quickstart";
    public static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    public static Gmail gmailService;
    public static final String EMAIL_PERBARINDO="perbarindo.org@gmail.com";
    public static final String URL_NOTIF_WA = "https://wapi.srv.co.id:31012/sendMessage";
    public static final String TOKEN_WA="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNjAyODMyOTM2fQ.q_Wg-mKlV8rOPds9bLvkYWQ-5s5WYJGTul35yCjUCTE";
    public static final String CONFIG_SCHEDULER="check_monthly_active_users";
    public static final String HEADER_SERVICE_ARI="Bearer $2y$12$bxQuFCcfF0jaa3NEpG8S1eMq61o58IlhJRvZcGrJOOvKAQXXI/HHS";

    //DEFAULT
    public static final String URL_CHECK_STATUS = "http://103.28.148.203:61005/sb/checkTerminalStatus";

    public static final String URL_GET_INVOICE = "http://10.81.20.30/api/sb/invoice";
    public static final String URL_INQUIRY = "https://lumen.srv.co.id/api/sb/inquiry";


    public static final String ZIP_FOLDER_NAME = "/Bunker/";


    //EMAIL
    public static final String URL_SEND_EMAIL="https://yunna-service.now.sh/v1/send_mail";
    public static final String HEADER_1_SEND_EMAIL="WIBUDEV-YUNNA2020";
    public static final String HEADER_2_SEND_EMAIL="4b490dee1673c324e82a232d591627d8";
    public static final String HEADER_3_SEND_EMAIL="4511bce5ec2f3f6ea93e317f81d128f1";


    public static JSONObject internalServerError() {
        return new JSONObject().put("rc", "99").put("rm", RM_IE);
    }

    public static boolean isAlphanumeric(String str) {
        char[] charArray = str.toCharArray();
        Boolean letter=false,digit=false;
        for (char c : charArray) {
            if(Character.isLetter(c))letter=true;
            if(Character.isDigit(c))digit=true;
        }
        if(letter && digit)return true;
        return false;
    }

    public static boolean isValidEmail(String email) {
        final EmailValidator emailValidator = new EmailValidator();
        return emailValidator.isValid(email, null);
    }

    public static boolean isContainCapital(String param) {
        if (param.matches(".*[A-Z].*")) return true;
        System.out.println("asd isCon false");
        return false;
    }

    public static boolean isContainSpecialCharacter(String inputString) {
        final Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
        final Matcher matcher = pattern.matcher(inputString);
        boolean isStringContainsSpecialCharacter = matcher.find();
        if (isStringContainsSpecialCharacter) {
            System.out.println("asd is spec true");
            return true;
        }else {
            System.out.println("asd is spec false");
            return false;
        }
    }

    public static int getDateDifferences(Date date1, Date date2){
        long diffInMillies = Math.abs(date1.getTime() - date2.getTime());
        return (int)TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static String getRandomNumberString() {
        final Random rnd = new Random();
        int number = rnd.nextInt(999999);
        return String.format("%06d", number);
    }

}
