/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.dak.perbarindo.sb.main.app.ManagementUser.Services;

import com.dak.perbarindo.sbdbcontroller.gateway.model.Lembaga;
import com.dak.perbarindo.sbdbcontroller.spring.SInit;
import java.util.List;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ilham
 */
public class GetAllLembaga {

    static final Logger log = LoggerFactory.getLogger("GetAllLembaga");

    public static JSONObject getAllLembaga(JSONObject reqBody) {
        List<Lembaga> lembagaData = SInit.getLembagaDao().getAllData();

        JSONObject jsonObject = new JSONObject();
        return jsonObject.put("data", lembagaData);
    }

    public static JSONObject getStatusLoginLembaga(JSONObject reqBody) {

        Integer status_login = reqBody.getInt("status_login");

        JSONObject response = new JSONObject();

        return response.put("data", SInit.getLembagaDao().getAllStatusLogin(status_login));
    }

    public static JSONObject updateStatusLoginLembaga(JSONObject reqBody) {

        Integer status_login = reqBody.getInt("status_login");
        Integer status_login_berubah = reqBody.getInt("status_login_berubah");
        JSONObject response = new JSONObject();

        List<Lembaga> lembaga = SInit.getLembagaDao().getAllStatusLogin(status_login);
        for (int i = 0; i < lembaga.size(); i++) {
            lembaga.get(i).setStatusLogin(status_login_berubah);

            SInit.getLembagaDao().saveorupdate(lembaga.get(i));
        }
        return response.put("status_login_berubah", status_login_berubah);
    }
}
